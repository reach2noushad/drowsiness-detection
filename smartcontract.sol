pragma solidity ^0.4.11;



contract ABC {
    
    uint[] public incidentIds;
 
    struct Drowsiness {
        
          uint id;
          uint vehicleId;
          uint time;
          address driver;
          uint acceleration;
          uint braking;
          
        }
        
      struct Driver {
          string name;
          address owner;
          uint[] drowsiness;
          address sharedTo;
          
        }
        
        struct Insurance {
          address owner;
          address sharedFrom;
          
        }
        
        struct LawAndOrder {
          address owner;
          
        }
        
    uint drowsinessId = 0;
    
    event incident (address driver,uint acceleration, uint braking, uint vehicleId, uint timestamp);
    event grantAccess(address sharedTo, uint timestamp);
    event revokeAccess (address sharedTo, uint timestamp);
    event accessRecordPolice(address user, uint timestamp);
    event accessRecordInsurance(address user, uint timestamp);
        
    mapping (uint=>Drowsiness) public drowsiness;
    mapping (address=>Driver) public driver;
    mapping (address=>Insurance) public insurance;
    mapping(address=>uint[]) public userArray;
    
        
    function logIncident(address _driver, uint _acceleration,uint _braking, uint _vehicleId, uint _timeStamp) public {
        
        drowsinessId++;
        
        drowsiness[drowsinessId].id = drowsinessId;
        drowsiness[drowsinessId].driver = _driver;
        drowsiness[drowsinessId].acceleration = _acceleration;
        drowsiness[drowsinessId].braking = _braking;
        drowsiness[drowsinessId].vehicleId = _vehicleId;
        drowsiness[drowsinessId].time = _timeStamp;
    
        incidentIds.push(drowsinessId);
        
        driver[_driver].drowsiness.push(drowsinessId);
        
        incident(_driver,_acceleration,_braking,_vehicleId,_timeStamp);
        
    } 
    
    function addSecondOwner(address _driver, address shareTo) public {
        
        driver[_driver].sharedTo = shareTo;
        insurance[shareTo].sharedFrom = _driver ;
        emit grantAccess(shareTo, now);
        
    }
    
    function removeSecondOwner(address _driver, address shareTo) public {
        
        driver[_driver].sharedTo = 0x00;
        insurance[shareTo].sharedFrom = 0x00;
        
        emit revokeAccess(driver[_driver].sharedTo, now);
        
    }
    
    
    function logAccessRecordPolice(address _accessingUser) public {
        emit accessRecordPolice(_accessingUser,now);
    }
    
    
    function logAccessRecordInsurance(address _accessingUser) public {
        
        emit accessRecordInsurance(_accessingUser,now);
        
        
    }

    
    function getIncidentIdsOfDriver(address _driver) public constant returns (uint[]) {
       return driver[_driver].drowsiness;
    }
    
    function getSharedUserAddress(address _insurance) public constant returns(address) {
        return insurance[_insurance].sharedFrom;
    }


    function getSharedInsuranceAddress(address _driver) public constant returns(address) {
        return driver[_driver].sharedTo;
    }
    
    
    function getIncident(uint _incidentId) public constant returns(address, uint, uint, uint, uint) {
       return (drowsiness[_incidentId].driver,drowsiness[_incidentId].acceleration,drowsiness[_incidentId].braking ,drowsiness[_incidentId].time ,drowsiness[_incidentId].vehicleId);
    }
   
}
