const ethers = require('ethers');
// const provider = ethers.providers.getDefaultProvider('rinkeby');

const provider = new ethers.providers.JsonRpcProvider('https://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551', 'rinkeby');

let contractAddress = "0x190251cDC322d7182a6D3Ffb57546219105CDCD0";
let abi = [{"constant":false,"inputs":[{"name":"_driver","type":"address"},{"name":"shareTo","type":"address"}],"name":"addSecondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"uint256"}],"name":"userArray","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_accessingUser","type":"address"}],"name":"logAccessRecordPolice","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"drowsiness","outputs":[{"name":"id","type":"uint256"},{"name":"vehicleId","type":"uint256"},{"name":"time","type":"uint256"},{"name":"driver","type":"address"},{"name":"acceleration","type":"uint256"},{"name":"braking","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_driver","type":"address"}],"name":"getSharedInsuranceAddress","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_incidentId","type":"uint256"}],"name":"getIncident","outputs":[{"name":"","type":"address"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_insurance","type":"address"}],"name":"getSharedUserAddress","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_driver","type":"address"},{"name":"shareTo","type":"address"}],"name":"removeSecondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"incidentIds","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_driver","type":"address"}],"name":"getIncidentIdsOfDriver","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_driver","type":"address"},{"name":"_acceleration","type":"uint256"},{"name":"_braking","type":"uint256"},{"name":"_vehicleId","type":"uint256"},{"name":"_timeStamp","type":"uint256"}],"name":"logIncident","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_accessingUser","type":"address"}],"name":"logAccessRecordInsurance","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"driver","outputs":[{"name":"name","type":"string"},{"name":"owner","type":"address"},{"name":"sharedTo","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"insurance","outputs":[{"name":"owner","type":"address"},{"name":"sharedFrom","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"driver","type":"address"},{"indexed":false,"name":"acceleration","type":"uint256"},{"indexed":false,"name":"braking","type":"uint256"},{"indexed":false,"name":"vehicleId","type":"uint256"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"incident","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"sharedTo","type":"address"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"grantAccess","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"sharedTo","type":"address"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"revokeAccess","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"user","type":"address"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"accessRecordPolice","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"user","type":"address"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"accessRecordInsurance","type":"event"}];

const Interface = ethers.Interface;
const iface = new Interface(abi);
const eventInfo = iface.events;

module.exports = {

    getIncidentEventLogs: async (eventTopic,eventName) => {

          var filter = {
            fromBlock: 	4017632,
            address: contractAddress,
            topics: [ eventTopic ]
         }

         var eventLogs = await provider.getLogs(filter);

    

        let a = [];

         for (const i in eventLogs) {

            let newEventInfo = eventInfo[eventName];

            var eventParsed = newEventInfo.parse(eventLogs[i].topics, eventLogs[i].data);

            let b = {};

            b.driver = eventParsed.driver;
            b.acceleration = eventParsed.acceleration.toNumber();
            b.braking = eventParsed.braking.toNumber();
            b.vehicleId = eventParsed.vehicleId.toNumber();
            b.time = eventParsed.timestamp.toNumber();
            b.driverName = "Siju";

            let date = new Date(eventParsed.timestamp.toNumber()*1000).toLocaleDateString();
            let time = new Date(eventParsed.timestamp.toNumber()*1000).toLocaleTimeString();
            b.date = date + " " + time;
            

            a.push(b);


         }

         return a;
   
    },

    getPoliceAccessLogs: async (eventTopic,eventName) => {

      var filter = {
        fromBlock: 	4017632,
        address: contractAddress,
        topics: [ eventTopic ]
     }

     var eventLogs = await provider.getLogs(filter);



    let a = [];

     for (const i in eventLogs) {

        let newEventInfo = eventInfo[eventName];

        var eventParsed = newEventInfo.parse(eventLogs[i].topics, eventLogs[i].data);

        console.log(eventParsed);

        let b = {};

        b.policeAddress = eventParsed.user;
        b.timestamp = eventParsed.timestamp.toNumber();
      

        let date = new Date(eventParsed.timestamp.toNumber()*1000).toLocaleDateString();
        let time = new Date(eventParsed.timestamp.toNumber()*1000).toLocaleTimeString();
        b.date = date + " " + time;
        

        a.push(b);


     }

     return a;

   },

   getInsuranceAccessLogs : async (eventTopic, eventName) => {


      var filter = {
         fromBlock: 	4017632,
         address: contractAddress,
         topics: [ eventTopic ]
      }
 
      var eventLogs = await provider.getLogs(filter);
 
 
 
     let a = [];
 
      for (const i in eventLogs) {
 
         let newEventInfo = eventInfo[eventName];
 
         var eventParsed = newEventInfo.parse(eventLogs[i].topics, eventLogs[i].data);
 
         console.log(eventParsed);
 
         let b = {};
 
         b.insuranceAddress = eventParsed.user;
         b.timestamp = eventParsed.timestamp.toNumber();
      
 
         let date = new Date(eventParsed.timestamp.toNumber()*1000).toLocaleDateString();
         let time = new Date(eventParsed.timestamp.toNumber()*1000).toLocaleTimeString();
         b.date = date + " " + time;
         
 
         a.push(b);
 
 
      }
 
      return a;



   }
}

