const ethers = require('ethers');
const keythereum = require("keythereum");
const os = require('os');
const helper = require('./helper');

// let provider = ethers.providers.getDefaultProvider('rinkeby');

let provider = new ethers.providers.JsonRpcProvider('https://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551', 'rinkeby');

let contractAddress = "0x190251cDC322d7182a6D3Ffb57546219105CDCD0";
let abi = [{"constant":false,"inputs":[{"name":"_driver","type":"address"},{"name":"shareTo","type":"address"}],"name":"addSecondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"uint256"}],"name":"userArray","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_accessingUser","type":"address"}],"name":"logAccessRecordPolice","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"drowsiness","outputs":[{"name":"id","type":"uint256"},{"name":"vehicleId","type":"uint256"},{"name":"time","type":"uint256"},{"name":"driver","type":"address"},{"name":"acceleration","type":"uint256"},{"name":"braking","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_driver","type":"address"}],"name":"getSharedInsuranceAddress","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_incidentId","type":"uint256"}],"name":"getIncident","outputs":[{"name":"","type":"address"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"},{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_insurance","type":"address"}],"name":"getSharedUserAddress","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_driver","type":"address"},{"name":"shareTo","type":"address"}],"name":"removeSecondOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"incidentIds","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"_driver","type":"address"}],"name":"getIncidentIdsOfDriver","outputs":[{"name":"","type":"uint256[]"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"_driver","type":"address"},{"name":"_acceleration","type":"uint256"},{"name":"_braking","type":"uint256"},{"name":"_vehicleId","type":"uint256"},{"name":"_timeStamp","type":"uint256"}],"name":"logIncident","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"_accessingUser","type":"address"}],"name":"logAccessRecordInsurance","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"driver","outputs":[{"name":"name","type":"string"},{"name":"owner","type":"address"},{"name":"sharedTo","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"insurance","outputs":[{"name":"owner","type":"address"},{"name":"sharedFrom","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"anonymous":false,"inputs":[{"indexed":false,"name":"driver","type":"address"},{"indexed":false,"name":"acceleration","type":"uint256"},{"indexed":false,"name":"braking","type":"uint256"},{"indexed":false,"name":"vehicleId","type":"uint256"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"incident","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"sharedTo","type":"address"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"grantAccess","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"sharedTo","type":"address"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"revokeAccess","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"user","type":"address"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"accessRecordPolice","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"user","type":"address"},{"indexed":false,"name":"timestamp","type":"uint256"}],"name":"accessRecordInsurance","type":"event"}];

let datadir = os.homedir();


module.exports = {

    getPrivateKey : async function(publicAddress){

        let address = publicAddress.slice(2);
        let keyObject = keythereum.importFromFile(address, datadir);
        let privateKey = keythereum.recover("password", keyObject);
        return privateKey;

    },

    logIncident : async function(data, privateKey) { 

        try {
            

        var wallet = new ethers.Wallet(privateKey,provider);
        var contract = new ethers.Contract(contractAddress,abi,wallet);

        let transaction = await contract.logIncident(data.driverAddress, data.acceleration, data.braking, data.vehicleId, data.time);
        await provider.waitForTransaction(transaction.hash);
        return true;

        } catch(e) {

            console.log(e);
            return e;
        }
    },

    giveInsuranceAccess : async function(driverAddress, insuranceAddress, privateKey) { 

        try {

            var wallet = new ethers.Wallet(privateKey,provider);
            var contract = new ethers.Contract(contractAddress,abi,wallet);

            let transaction = await contract.addSecondOwner(driverAddress, insuranceAddress);
            await provider.waitForTransaction(transaction.hash);
            return true;

        } catch (e) {

            console.log(e);
            return e;
        }

    },

    revokeInsuranceAccess : async function(driverAddress, insuranceAddress, privateKey) { 

        try {

            var wallet = new ethers.Wallet(privateKey,provider);
            var contract = new ethers.Contract(contractAddress,abi,wallet);

            let transaction = await contract.removeSecondOwner(driverAddress, insuranceAddress);
            await provider.waitForTransaction(transaction.hash);
            return true;

            //need to test this after deploying the new contract

        } catch (e) {

            console.log(e);
            return e;
        }

    },

    getIncidentIds : async function(driverAddress) { 

        try {

            var contract = new ethers.Contract(contractAddress,abi,provider);

            return await contract.getIncidentIdsOfDriver(driverAddress);

        } catch (e) {

            console.log(e);
            return e;

        }

    },

    getIncident : async function(id) { 

        try {

            var b = {};

            var contract = new ethers.Contract(contractAddress,abi,provider);

            const incident = await  contract.getIncident(id);

            b.address = incident[0];
            b.name = "Siju";
            b.acceleration = incident[1].toNumber();
            b.braking = incident[2].toNumber();
            b.time = incident[3].toNumber();
            b.vehicleId = incident[4].toNumber();

            return b;


            

        } catch (e) {

            console.log(e);
            return e;

        }

    },

    getSharedAddress : async function(insuranceAddress) { 

        try {

            var contract = new ethers.Contract(contractAddress,abi,provider);
            return await contract.getSharedUserAddress(insuranceAddress);
                       

        } catch (e) {

            console.log(e);
            return e;

        }

    },

    getInsurer: async function(driver) { 

        try {

            var contract = new ethers.Contract(contractAddress,abi,provider);
            return await contract.getSharedInsuranceAddress(driver);
                       

        } catch (e) {

            console.log(e);
            return e;

        }

    },

    logPoliceAccessingRecord : async function(policeAddress,privateKey) { 

        try {

            var wallet = new ethers.Wallet(privateKey,provider);
            var contract = new ethers.Contract(contractAddress,abi,wallet);

            const emitEvent = await contract.logAccessRecordPolice(policeAddress);
            

        } catch (e) {

            console.log(e);
            return e;

        }

    },


    logInsuranceAccessingRecord : async function(insuranceAddress,privateKey) { 

        try {

    
            var wallet = new ethers.Wallet(privateKey,provider);
            var contract = new ethers.Contract(contractAddress,abi,wallet);

            const emitEvent = await contract.logAccessRecordInsurance(insuranceAddress);
            

        } catch (e) {

            console.log(e);
            return e;

        }

    },

    approveHouseSell : async function(houseId, privateKey) { 

        try {

            var wallet = new ethers.Wallet(privateKey,provider);
            var contract = new ethers.Contract(contractAddress,abi,wallet);

            console.log(+houseId);
            console.log(typeof(houseId));

            //get index of house ID in muncipality list
            muncipalityIdBN = await contract.getMuncipalityHouseIds();
            const muncipalityId = muncipalityIdBN.map(id => id.toNumber());
            console.log(muncipalityId);
            var index = muncipalityId.indexOf(parseInt(houseId));

            console.log(index);
   
            let transaction = await contract.approveHouseSell(houseId,index);
            await provider.waitForTransaction(transaction.hash);
            return true;

        } catch (e) {

            console.log(e);
            return e;

        }

    },

    getHouseIds : async function() { 

        try {

            var contract = new ethers.Contract(contractAddress,abi,provider);

            return await contract.getAllHouseIds();

        } catch (e) {

            console.log(e);
            return e;

        }

    },

    getMuncipalityHouseIds : async function() { 

        try {

            var contract = new ethers.Contract(contractAddress,abi,provider);

            return await contract.getMuncipalityHouseIds();

        } catch (e) {

            console.log(e);
            return e;

        }

    },

    getHouse : async function(houseId) { 

        try {

            var contract = new ethers.Contract(contractAddress,abi,provider);

            return await contract.getHouse(houseId);

        } catch (e) {

            console.log(e);
            return e;

        }

    },

    addUserDetails : async function(address,name,userType, privateKey) { 

        try {

            var wallet = new ethers.Wallet(privateKey,provider);
            var contract = new ethers.Contract(contractAddress,abi,wallet);

            let transaction = await contract.addUSer(address,name,userType);
            await provider.waitForTransaction(transaction.hash);
            return true;

        } catch (e) {

            console.log(e);
            return e;

        }

    },

    getUserDetails : async function(address) { 

        try {

            var contract = new ethers.Contract(contractAddress,abi,provider);

            return await contract.user(address);

        } catch (e) {

            console.log(e);
            return e;

        }

    }



}

