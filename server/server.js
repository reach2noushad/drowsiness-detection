const restify = require('restify');
const rp = require('request-promise');
const transaction = require('./transaction');
const helper = require('./helper');
const eventsApi = require('./events');
const corsMiddleware = require('restify-cors-middleware')
const auth_token= "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6Ik5jUW5FdGRzcnhpTEFuWXhrT0RmUUg0VFVjayIsIng1dCI6Ik5jUW5FdGRzcnhpTEFuWXhrT0RmUUg0VFVjayJ9.eyJpc3MiOiJodHRwczovL2lkZW50aXR5LmNhbnRpei5jb20iLCJhdWQiOiJodHRwczovL2lkZW50aXR5LmNhbnRpei5jb20vcmVzb3VyY2VzIiwiZXhwIjpudWxsLCJuYmYiOjE1MTYyNjk0ODAsInNjb3BlIjpbIm9wZW5pZCJdLCJhdXRoVGltZSI6MTUxNjI2OTQ4MCwiaWRwIjoiaWRzcnYiLCJ0eXBlIjoiYXBpX2tleSIsImFjY291bnRJZCI6Ijc2MiIsImtleU5hbWUiOiJBUElfS0VZXzEzMSIsImtleUlkIjoiMTA3IiwicmVnZW5lcmF0ZWQiOiJGYWxzZSIsIm9sZEtleUlkIjpudWxsLCJjdXN0b21JbmZvcm1hdGlvbiI6IntcIm51Y2xldXNBY2NvdW50SWRcIjpcImRoYjFuYTYxYzUzN1wiLFwibnVjbGV1c0FQSUtleUlkXCI6XCIxMzFcIn0ifQ.slpiNlt6Ftdq8Qg9Rd_wjXmomZioDa9KsZFCaDwRQdykm3G3--o-ajuv4B2mhMqMXPUA1w2pCPluw-kzcro79goAabTeiSBYF4NSNIyiyI0V2koLmRGz_uA_1sOCn0kMjQKza9pBEN4n-HfvK6ykLsjbv9_GKDA8fKLylFMTXzgGi20F6ta41eXl5NTBDTg6FvLVXCS3_Sy7WtkioRnrBOUV80MghjEjjqp62P4ZB-6mZzClQz2ERR8VnnwoG8u3xXvGKaEJahOWiUSK9WNsfsU6ZYBnvMvoWYBMu43F3vT1tCNfMCbC58iURq0vwSzbxcOSC-cSoO0shfOHLMMHKg";
const cors = corsMiddleware({
  origins: ['http://localhost:4200','http://115.114.36.146:4300', 'http://digitaldriver.cantiz.com:4300'],
  allowHeaders: ['*']
})

const server = restify.createServer({
  name: 'myapp',
  version: '1.0.0'
});

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());
server.pre(cors.preflight);
server.use(cors.actual);
async function getNucleusData(timestamp) {
  console.log("timestamp", timestamp);
    const fromTime = Math.floor(Number(timestamp)/100)*100; 
    const toTime = Math.ceil(Number(timestamp)/100)*100;
    console.log(fromTime, toTime);
    var options = {
        url: `http://gateway.nucleus.cantiz.com:8080/accounts/dhb1na61c537/things/v2vjrgrvka2u/events?from-time=${fromTime}&to-time=${toTime}`,
        headers: {
          'Authorization': auth_token
        },
        json: true 
    };
    let acceleration_sum = 0, braking_sum = 0;
    return rp(options)
        .then(function (response) {
            console.log('User has data', response.data);
            // response.data.forEach(d => {
            //   console.log(d.attributes.breaking)
            //   acceleration_sum += parseFloat(d.attributes.acceleration);
            //   braking_sum += parseFloat(d.attributes.breaking);
            // });
            // console.log(acceleration_sum/response.data.length, braking_sum/response.data.length);
            return {Acceleration:response.data[0].attributes.acceleration, Braking : response.data[0].attributes.breaking};
        })
        .catch(function (err) {
          console.log(err);
        });

    


}

server.post('/logIncident', async (req, res, next) => {

  if(req.body && req.body.driverId) {

    const data =  {
        driverId:  req.body.driverId,
        vehicleId: req.body.vehicleId,
        time: req.body.timeStamp
    }
    const nucleusData = await getNucleusData(req.body.timeStamp);
    console.log(nucleusData);
    data.acceleration = nucleusData.Acceleration.toFixed(4) *10000;
    data.braking = nucleusData.Braking.toFixed(4) *10000;
    data.driverAddress = helper.addresses.driver;
    const privateKey = await transaction.getPrivateKey(data.driverAddress);

    const drowsinessDetection = await transaction.logIncident(data, privateKey);

    res.send(drowsinessDetection);

    } else {

        res.send({message: "Missing Driver ID"});
    }


});

server.post("/insurance/add", async (req, res, next) =>  {


    console.log("granting access to insurance person");
   
    insuranceAddress = helper.addresses.insurance;
    driverAddress = helper.addresses.driver;
    const privateKey = await transaction.getPrivateKey(driverAddress);
    const grantAccessToInsurance = await transaction.giveInsuranceAccess(driverAddress,insuranceAddress,privateKey);
    res.send(grantAccessToInsurance);
    

});

server.post("/insurance/revoke", async (req, res, next) => {

    insuranceAddress = helper.addresses.insurance;
    driverAddress = helper.addresses.driver;
    const privateKey = await transaction.getPrivateKey(driverAddress);

    const revokeAccessToInsurance = await transaction.revokeInsuranceAccess(driverAddress,insuranceAddress,privateKey);
    res.send(revokeAccessToInsurance);

    
});

server.get("/driver/incidents", async (req, res, next) => {

    console.log("getting driver incidents");

    driverAddress = helper.addresses.driver;

    const driverIncidentIds = await transaction.getIncidentIds(driverAddress);
    var BNtonumber = driverIncidentIds.map(incident => incident.toNumber());


    var a = [];

    for (const i in BNtonumber){
        
        const incident = await transaction.getIncident(BNtonumber[i]);
        a.push(incident);
    }
    console.log(a);
    res.send({"data":[{"user":"Siju","info":a}]});
    
});


server.get("/police/driver/incidents", async (req, res, next) => {

    console.log("getting driver incidents by police, need to fire event");

    driverAddress = helper.addresses.driver;
    policeAddress = helper.addresses.police;

    const privateKey = await transaction.getPrivateKey(policeAddress);

    transaction.logPoliceAccessingRecord(policeAddress,privateKey);
    const driverIncidentIds = await transaction.getIncidentIds(driverAddress);
    var BNtonumber = driverIncidentIds.map(incident => incident.toNumber());


    var a = [];

    for (const i in BNtonumber){
        
        const incident = await transaction.getIncident(BNtonumber[i]);
        a.push(incident);
    }
    console.log(a);
    res.send({"data":[{"user":"Siju","info":a}]});
    
});

server.get("/accessGivenAddresses", async (req, res, next) => {

    insuranceAddress = helper.addresses.insurance;

    
    var sharedAddress = await transaction.getSharedAddress(insuranceAddress);

    console.log(sharedAddress);

    const driverIncidentIds = await transaction.getIncidentIds(sharedAddress);
    var BNtonumber = driverIncidentIds.map(incident => incident.toNumber());


    var a = [];

    for (const i in BNtonumber){
        
        const incident = await transaction.getIncident(BNtonumber[i]);
        a.push(incident);
    }
    console.log(a);
    if (sharedAddress == "0x0000000000000000000000000000000000000000") {
      res.send({message : "No user has shared address"});
  } else {
    const privateKey = await transaction.getPrivateKey(insuranceAddress);
    transaction.logInsuranceAccessingRecord(insuranceAddress,privateKey);
    res.send({"data":[{"user":"Siju","info":a}]});
  }
    
    
 
    
});

server.post("/getSharedUserData", async (req, res, next) => {

    let address = req.body.driverAddress;

    const driverIncidentIds = await transaction.getIncidentIds(address);
    var BNtonumber = driverIncidentIds.map(incident => incident.toNumber());

    var a = [];

    for (const i in BNtonumber){
        
        const incident = await transaction.getIncident(BNtonumber[i]);
        a.push(incident);
    }

    res.send({"data":[{"user":"Siju","info":a}]});
    
});

server.get("/auditLogOfIncidents", async (req, res, next) => {

    console.log("calling audit log api called by police, no need to fire event");

    let eventTopic = "0xc7457bc74239e4266e41ac757e7d80cc99b6c97ef0e0caf5b19944887ad67055";
    let eventName = "incident";



    const events = await eventsApi.getIncidentEventLogs(eventTopic,eventName);

    res.send(events);
 
    
});

server.get("/auditLogOfPoliceAccess", async (req, res, next) => {

    console.log("calling audit log of access made by police");

    let eventTopic = "0xf4b23d526aa95b661f6fcc1b7be9c6039a1799707bdbba7dba15b13699db27c2";
    let eventName = "accessRecordPolice";



    const events = await eventsApi.getPoliceAccessLogs(eventTopic,eventName);

    res.send(events);
 
    
});


server.get("/auditLogOfInsuranceAccess", async (req, res, next) => {

    console.log("calling audit log of access made by Insurance");

    let eventTopic = "0xabc343ff9a087187f3030f6750d0f8ea0983315f9a1088e19c322deab81ceae5";
    let eventName = "accessRecordInsurance";

    const events = await eventsApi.getInsuranceAccessLogs(eventTopic,eventName);

    res.send(events);
 
    
});


server.get("/getInsurerDetails", async (req, res, next) => {

  insuranceAddress = helper.addresses.driver;

  var sharedAddress = await transaction.getInsurer(insuranceAddress);

  console.log(sharedAddress);

  if (sharedAddress == "0x0000000000000000000000000000000000000000") {
    res.send({message : "You have not shared data to any insurers"});
} else {
  res.send([{sharedAddress}]);
}


  
});


server.listen(4400, function () {
  console.log('%s listening at %s', server.name, server.url);
});
