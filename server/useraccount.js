const keythereum = require("keythereum");
const os = require('os');
const ethers = require('ethers');
const mkdirp = require('mkdirp');

// let provider = ethers.providers.getDefaultProvider('rinkeby');
var provider = new ethers.providers.JsonRpcProvider('https://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551', 'rinkeby');
let prefix = os.homedir();
let keyStorePath = prefix+'/keystore';

participants = ["driver","police","insurance","admin"];


async function loadUsers() {

    for (const i in participants) {

        await mkdirp(keyStorePath);

        //Creating keystore
        params = { keyBytes: 32, ivBytes: 16 };
        dk = keythereum.create(params);
        password = "password";
        kdf = "pbkdf2";
        options = {kdf: "pbkdf2",cipher: "aes-128-ctr",kdfparams: {c: 262144,dklen: 32,prf: "hmac-sha256"}};
        var keyObject = keythereum.dump(password, dk.privateKey, dk.salt, dk.iv, options);

        //saving Keystore File in /home/{username}/keystore folder
        keythereum.exportToFile(keyObject,keyStorePath);

        var publicAddress = "0x"+keyObject.address;

        //loading ether to account

            var privateKey = '0x1B374987FA51AE90B47E30A0F88639FA6F96D758D7D77F4530F17FF1CF7C1673';
            var wallet = new ethers.Wallet(privateKey);
            // wallet.provider = ethers.providers.getDefaultProvider('rinkeby');
            wallet.provider = new ethers.providers.JsonRpcProvider('https://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551', 'rinkeby');
            //Amount of Ethers To Be Loaded(0.5 in this case)
            var amount = ethers.utils.parseEther('0.5');
            const sendPromise = await wallet.send(publicAddress, amount);
            await provider.waitForTransaction(sendPromise.hash);
            console.log(participants[i] + " has address " + publicAddress+" loaded with 0.1 Ethers");

    }

}


loadUsers();