import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { User, Provider } from './user';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthenticationService {

  public loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public userType: BehaviorSubject<string> = new BehaviorSubject<string>("");
  userProfile: any;
  user: string = "0x2d110f1f9cb32efba577cf8ca4f93f43695e2f32";
  police: string = "0x3278a5dd93c7d460c51929f92cdf6713f4454d71";
  insurance: string = "0x25ed4ebc688be1c79dac416f3a211bc51d8ccc6a";
  constructor(private router: Router, private http: HttpClient) { }

  get isLoggedIn() {
    return this.loggedIn.asObservable();
  }

  get userInitialized() {
    return this.userType.asObservable();
  }

  generateOTP(data): Observable<any> {
    console.log("registerUser")
    return this.http.post(`http://115.114.36.146:4445/api/user/generateotp`, data)
      .pipe(map(response => { console.log(response); return response; }))
  }

  login(user: User): Observable<any> {
    console.log("login", user)
    return this.http.post(`http://115.114.36.146:4445/api/user/login`, user)
      .pipe(map(response => {
        this.loggedIn.next(true);
        this.userType.next(user.user);
        return response;
      })
      );
  }

logout() {
  this.loggedIn.next(false);
  this.router.navigate(['/signin']);
}

getProviders(): Observable < Provider[] > {
  return this.http.get<Provider[]>(`http://115.114.36.146:4445/api/providers`)
    .pipe(map(response => { console.log(response); return response }))
}

signup(data): Observable < any > {
  return this.http.post(`http://115.114.36.146:4445/api/user/register`, data)
    .pipe(map(response => { console.log(response); return response }))
}

getRegisteredProviders(data) : Observable < any > {
  console.log("calling get providers api from front end");
  console.log(data);
  let queryParams='';
  if(data.ssn){
  queryParams = `?userId=${data.ssn}`;
}
    else if (data.address) {
  queryParams = `?address=${data.address}`;
}

return this.http.get(`http://115.114.36.146:4445/api/user/providerList` + queryParams)
  .pipe(map(response => {
    console.log(response);
    return response;
  }));
  }
}
