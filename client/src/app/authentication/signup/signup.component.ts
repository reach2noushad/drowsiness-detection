import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { User } from './../services/user';
import { Router } from '@angular/router';
import { Component, OnInit } from "@angular/core";
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: "app-signup",
  templateUrl: "./signup.component.html",
  styleUrls: ["./signup.component.scss"]
})
export class SignupComponent implements OnInit {

  providers: Array<any>;
  provider: string;
  user:User =  <User>new Object();
  showOtpform: boolean = false;
  otp: number = null;
  signupForm2 = false;
  constructor(private router: Router, 
        private authService: AuthenticationService,
        private toastr: ToastrService) {


  }

  ngOnInit() {
    this.getProviders();
  }

  getProviders(): void {
    this.authService.getProviders()
      .subscribe(response => {
        this.providers = response['providers'];
        this.provider = this.providers[0];
      },error => {
        console.log(error);
      });
  }



  nextSignup2() {
    this.signupForm2 = true;
  }
  nextSignup1() {
    this.signupForm2 = false;
  }

  complete() {
    this.user.isRegistered = false;
    this.user.provider = this.provider;
    console.log(this.user);
    this.authService.generateOTP(this.user).subscribe(response => {
      if (response) {
        this.toastr.success("Success!!!", "OTP generated successfully");
        console.log(response);
        this.showOtpform=true;
      }
    },
      error => {
        this.toastr.error("OTP generation failed", "Something went wrong. Please try again after sometime.");
       console.log(error);
      });
  }
  signup () {
    this.user.otp = this.otp;
    this.authService.signup(this.user)
      .subscribe(response => {
        if (response) {
          console.log(response);
          this.toastr.success("Success!!!", "Your account has been successfully created");
        }
      },
        error => {
          this.toastr.error("Registration failed!!!", "Something went wrong. Please try again after sometime.");
         console.log(error);
        });
  }
}
