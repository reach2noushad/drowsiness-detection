import { AuthenticationService } from './authentication/services/authentication.service';
import { AuthGuard } from './authentication/auth.guard';

import { AuthenticationModule } from './authentication/authentication.module';
import { DashboardModule } from './home/dashboard/dashboard.module';
import { HomeModule } from './home/home.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material/angular-material.module';
import { ModalComponent } from './modal/modal.component';
import { PatientModule } from './home/dashboard/patient/patient.module';
import {AppSideNavComponent} from './home/app-side-nav/app-side-nav.component';
import { HttpClientModule } from '@angular/common/http';
import { ModalModule } from './modal/modal.module';
import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from 'ngx-spinner';
@NgModule({
  declarations: [
    AppComponent,
    AppSideNavComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    ModalModule,
    DashboardModule,
    PatientModule,
    AuthenticationModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
    HttpClientModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      closeButton: true
    }),
    NgxSpinnerModule
  ],
  providers: [AuthenticationService, AuthGuard],
  bootstrap: [AppComponent],
  entryComponents:[ModalComponent]
})
export class AppModule { }
