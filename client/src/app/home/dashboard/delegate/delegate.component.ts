import { HomeService } from './../../home.service';
import { AuthenticationService } from './../../../authentication/services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ModalComponent } from 'src/app/modal/modal.component';
import { NgxSpinnerService } from 'ngx-spinner';
@Component({
  selector: 'app-delegate',
  templateUrl: './delegate.component.html',
  styleUrls: ['./delegate.component.scss']
})
export class DelegateComponent implements OnInit {

  nominees: any;
  user: any
  givers: any;
  grantors: any;
  insurers: any;
  insurance: string;
  useraddress: string;
  constructor(
    private authService: AuthenticationService,
    private homeService: HomeService,
    public dialog: MatDialog,
    private spinner: NgxSpinnerService) { }

  ngOnInit() {
    let user = this.authService.userProfile;
    //temporary
    this.user = user;
    this.useraddress = this.authService.user;
    this.getInsurers();
  }

  getInsurers(): void {
    this.spinner.show()
    this.homeService.getInsurers(this.authService.user).subscribe(response => {
      console.log("response", response);
      if(response && Array.isArray(response)) {
        this.insurers = response;
        this.spinner.hide()
      }
      else {
        this.insurers = null;
        this.spinner.hide()
      }
      
    },
      error => {
        console.log("error", error);
      });
  }

  getNominees(user = null): void {
    this.homeService.getNominees(user).subscribe(response => {
      console.log("response", response);
      this.nominees = response;
    },
      error => {
        console.log("error", error);
      });
  }

  // getGrantor(user = null): void {
  //   this.homeService.getGrantor(user).subscribe(response => {
  //     console.log("response", response);
  //     this.grantors = response;
  //   },
  //     error => {
  //       console.log("error", error);
  //     });
  // }

  delegate(): void {
    this.spinner.show()
    this.insurance = this.authService.insurance;
    this.homeService.delegate({insurance: this.insurance, driver: this.useraddress}).subscribe(response => {
      console.log("response", response);
      this.dialog.closeAll();
      this.getInsurers();
    },
      error => {
        this.spinner.hide()
        console.log("error", error);
      });
    // const data = { user_id: this.user.creds.ssn, nominee_id: nominee }
    // this.homeService.delegate(data).subscribe(response => {
    //   console.log("response", response);
    //   this.nominees = response;
    // },
    //   error => {
    //     console.log("error", error);
    //   });
  }

  revoke(insurance): void {
    this.spinner.show()
    this.homeService.revokeDelegation({insurance: insurance, driver: this.useraddress}).subscribe(response => {
      console.log("response", response);
      this.dialog.closeAll();
      this.getInsurers();
    },
      error => {
        this.spinner.hide()
        console.log("error", error);
      });
  }

  manageDoctorAccess(value, doctor): void {
    let data = {
      user_id: this.user.creds.ssn,
      user_name: this.user.creds.name,
      nominator_id: value.grantor.Adhaar,
      doctorAddress: doctor,
      grantAccess: value.mode
    }
    this.homeService.grantAccess(data)
      .subscribe(response => {
        console.log("response", response);
        if (value.mode) {
          // info = {
          //   title: "Success",
          //   subTitle: "Access shared with doctor for " + nominator.name
          // }

        }
        else {
          // info = {
          //   title: "Success",
          //   subTitle: "Access revoked from doctorfor " + nominator.name
          // }
        }
      },
        error => {
          // let info = {
          //   title: "Error",
          //   subTitle: "Oops!!! Something went wrong. Please try again after sometime"
          // }
          console.log("error", error);
        });
  }

  openModal(data) {
    const dialogRef = this.dialog.open(ModalComponent, { data });
    const onDelegate = dialogRef.componentInstance.onDelegate.subscribe((nominee) => {
      this.delegate();
    })
    const onDoctorDelegation = dialogRef.componentInstance.onDoctorDelegation.subscribe((doctor) => {
      console.log(data, doctor);
      this.manageDoctorAccess(data, doctor)
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      onDelegate.unsubscribe();
      onDoctorDelegation.unsubscribe();
    });
  }

}
