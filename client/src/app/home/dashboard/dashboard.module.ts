import { PatientModule } from './patient/patient.module';
import { AngularMaterialModule } from '../../angular-material/angular-material.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DoctorComponent } from './doctor/doctor.component';
import { DelegateComponent } from './delegate/delegate.component';
import { DetailsComponent } from './details/details.component';
import { ProfileComponent } from './profile/profile.component';
//import { DashboardRoutingModule } from './dashboard-routing.module';
@NgModule({
  declarations: [DashboardComponent, DoctorComponent, DelegateComponent, DetailsComponent, ProfileComponent],
  imports: [
    CommonModule, AngularMaterialModule/*, DashboardRoutingModule*/, PatientModule
  ],
  exports:[
    DashboardComponent
  ]

})
export class DashboardModule { }
