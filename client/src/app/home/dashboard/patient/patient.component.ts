import { AuthenticationService } from 'src/app/authentication/services/authentication.service';
import { HomeService } from './../../home.service';
import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ModalComponent } from 'src/app/modal/modal.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit {

  @Input() user;
  patients: any;
  providers: any;
  selectedAddress: any;
  patientDetails: any;
  provider: any;
  visits: any;
  observations: any;
  prescriptions: any;
  selectedPatient: any;
  incidents: any;
  insurance: string;
  useraddress: any;
  insurers: any[];
  constructor(public dialog: MatDialog, 
    private homeService: HomeService, 
    private authService: AuthenticationService,
    private spinner: NgxSpinnerService,
    private router: Router ) {
    console.log(this.user);
   }

  ngOnInit() {
    this.user = this.authService.userProfile;
    this.useraddress = this.authService.user;
    this.insurance = this.authService.insurance;
    if(this.user !== "rta")
      this.getIncidents();
    else 
    this.getInsurers();
  }

  openModal(data: any) {
    const dialogRef = this.dialog.open(ModalComponent, {data});
    // const onPatientDataFetch = dialogRef.componentInstance.onPatientDataFetch.subscribe(provider => {
    //   console.log(provider);
    //   this.getDemographics(provider);
    // });

    // dialogRef.afterClosed().subscribe(result => {
    //   this.selectedPatient.isShown = !this.selectedPatient.isShown ? false: true; 
    //   onPatientDataFetch.unsubscribe();
    // });
  }

  download()  {
    console.log("hello")
  }

  togglePanel(expanded) {
    expanded = !expanded;
  }

  getPatients(ssn): void {
    this.homeService.getAllocatedUsers(ssn, null).subscribe(response => {
      this.patients = response;
      this.patients.map(p =>  {p.isShown = false});
      console.log(this.patients);
    },
    error => {
      console.log(error);
    })
  }

  getIncidents() : void  {
    this.spinner.show();
    if(this.user === "police") {
      this.homeService.getIncidents().subscribe(response => {
        this.incidents = response;
        if(this.incidents && this.incidents.data)this.incidents.data.map(i =>  {i.isShown = false});
        console.log(this.incidents);
        this.spinner.hide();
      },
      error => {
        console.log(error);
        this.spinner.hide();
      })
    }
    else {
      this.homeService.getUsers().subscribe(response => {
        this.incidents = response;
        if(this.incidents && this.incidents.data)this.incidents.data.map(i =>  {i.isShown = false});
        console.log(this.incidents);
        this.spinner.hide();
      },
      error => {
        console.log(error);
        this.spinner.hide();
      })
    }


    
    // this.homeService.getIncidents().subscribe(response => {
    //   this.incidents = response;
    //   this.incidents.data.map(i =>  {i.isShown = false});
    //   console.log(this.incidents);
    // },
    // error => {
    //   console.log(error);
    // })
  }

  delegate(): void {
    this.spinner.show()
    this.insurance = this.authService.insurance;
    this.homeService.delegate({insurance: this.insurance, driver: this.useraddress}).subscribe(response => {
      console.log("response", response);
      this.dialog.closeAll();
      this.getInsurers();
    },
      error => {
        this.spinner.hide()
        console.log("error", error);
      });
    // const data = { user_id: this.user.creds.ssn, nominee_id: nominee }
    // this.homeService.delegate(data).subscribe(response => {
    //   console.log("response", response);
    //   this.nominees = response;
    // },
    //   error => {
    //     console.log("error", error);
    //   });
  }

  revoke(insurance): void {
    this.spinner.show()
    this.homeService.revokeDelegation({insurance: insurance, driver: this.useraddress}).subscribe(response => {
      console.log("response", response);
      this.dialog.closeAll();
      this.getInsurers();
    },
      error => {
        this.spinner.hide()
        console.log("error", error);
      });
  }

  getInsurers(): void {
    this.spinner.show()
    this.homeService.getInsurers(this.authService.user).subscribe(response => {
      console.log("response", response);
      if(response && Array.isArray(response)) {
        this.insurers = response;
        this.spinner.hide()
      }
      else {
        this.insurers = null;
        this.spinner.hide()
      }
      
    },
      error => {
        console.log("error", error);
      });
  }

  goToPage (){
    window.open("http://digitaldriver.cantiz.com:4600", "_blank");
  }

  // getProviders(address, patient) : void {
  //   if(!patient.isShown) {
  //     this.selectedAddress = address;
  //     this.homeService.getProvidersOfUser({ address }).subscribe(response => {
  //       console.log(response);
  //       this.providers = response;
  //       this.selectedPatient = patient;
  //       this.openModal({type:'providers', providers: this.providers});
  //     },
  //       error => {
  //         console.error('Oops:', error);
          
  //       });
  //   }
  // }

  // getDemographics(provider) : void {
  //   this.provider = provider;
  //   this.homeService.getDemographics(this.selectedAddress, this.user.user.account, provider.providerNumber).subscribe(response => {
  //     console.log("shared address");
  //     console.log(response);
  //     this.selectedPatient.isShown = true;
  //     this.patientDetails = JSON.parse(response.body).entry[0].resource;
  //     this.patientDetails.selectedAddress = this.selectedAddress;
  //   },
  //     error => {
  //       console.error('Oops:', error);
  //     });
  // }

  // getPatientVisits(patient) : void {
  //   this.homeService.getVisits(patient.identifier[0].value, this.provider.providerNumber).subscribe(visits => {
  //     console.log(visits);
  //     this.visits = visits.entry;
  //     this.openModal({type:'visits', visits: this.visits});
  //   })
  // }

  // getObservations(patient) {
  //   this.homeService.getObservations(patient.id, this.provider.providerNumber).subscribe(observations => {
  //     console.log(observations);
  //     this.observations = observations.entry;
  //     this.openModal({type:'observations', observations: this.observations});
  //   })
  // }

  // getPrescriptions(patient) {
  //   this.homeService.getPrescriptions(patient.id, this.provider.providerNumber).subscribe(response => {
  //     console.log(response);
  //     this.prescriptions = response.prescriptions;
  //     this.openModal({type:'prescriptions', prescriptions: this.prescriptions});
  //   })
  // }

  convertToTime(time) {
    return new Date(time).toLocaleString();
  }

}
