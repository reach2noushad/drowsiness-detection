import { AuthenticationService } from './../../../authentication/services/authentication.service';
import { HomeService } from './../../home.service';
import { Component, OnInit } from "@angular/core";
import { ViewEncapsulation } from "@angular/core";
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: "app-details",
  templateUrl: "./details.component.html",
  styleUrls: ["./details.component.scss"],
  encapsulation: ViewEncapsulation.None
})
export class DetailsComponent implements OnInit {

  visits: any;
  observations: any;
  prescriptions: any;
  address: string;
  incidents: any;
  incidentLog: any;
  constructor(private userService: HomeService, private authService: AuthenticationService,
    private spinner: NgxSpinnerService) {
    
  }

  ngOnInit() {
    this.spinner.show();
    this.address = this.authService.user;
    // let profileInfo = this.getUserProfile();
    // let user = profileInfo ? profileInfo.creds: null;
    // let demographics = profileInfo.clinicalData.body.entry[0].resource;
    // let emrNumber = user.emrDetails.number;
    // this.userService.getVisits(demographics.identifier[0].value, emrNumber).subscribe(visits => {
    //   console.log(visits);
    //   this.visits = visits.entry;
    //   this.userService.getObservations(demographics.id, emrNumber).subscribe(observations => {
    //     console.log(observations);
    //     this.observations = observations.entry;
    //     this.userService.getPrescriptions(demographics.id, emrNumber).subscribe(response => {
    //       console.log(response);
    //       this.prescriptions = response.prescriptions;
    //     })
    //   })
    // })
    if(this.authService.userProfile === "user" || this.authService.userProfile === "rta") {
      console.log("from user dashboard");
      this.userService.getABCData().subscribe(data => {
        console.log(data);
        this.incidentLog = null
        this.incidents = data;
        this.spinner.hide();
        //   console.log(visits);
        //   this.visits = visits.entry;
        //   this.userService.getObservations(demographics.id, emrNumber).subscribe(observations => {
        //     console.log(observations);
        //     this.observations = observations.entry;
        //     this.userService.getPrescriptions(demographics.id, emrNumber).subscribe(response => {
        //       console.log(response);
        //       this.prescriptions = response.prescriptions;
        //     })
        //   })
      }, 
      error => {
        console.log(error);
        this.spinner.hide();
      });
    }
    else {
      console.log("from police dashboard");
      this.userService.getIncidentLog().subscribe(data => {
        console.log(data);
        this.incidents = null
        this.incidentLog = data;
        this.spinner.hide();
      }, 
      error => {
        console.log(error);
        this.spinner.hide();
      });
    }
    
  }

  getUserProfile() {
    return this.authService.userProfile;
  }

  convertToTime(time) {
    return new Date(time).toLocaleString();
  }
}
