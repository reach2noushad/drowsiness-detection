import { Component, OnInit, Input } from '@angular/core';
import {MatDialog} from '@angular/material';
import { ModalComponent } from '../../modal/modal.component';
import { NavbarService } from 'src/app/shared/sevices/navbar/navbar.service';
//Learn more about this
//import { userInfo } from 'os';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  @Input() user;
  role: string;
  constructor(public dialog: MatDialog, private nav: NavbarService) {
    this.role = this.user;
    //get user role from API
    this.role = "doctor";
   }

  ngOnInit() {
    this.nav.show();
  }

  openModal() {
    const dialogRef = this.dialog.open(ModalComponent);

    dialogRef.afterClosed().subscribe(result => {
      //console.log(`Dialog result: ${result}`);
    });
  }

}
